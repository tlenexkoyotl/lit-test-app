import { LitElement, html, css, supportsAdoptingStyleSheets } from 'lit';
import { theme } from '../styles/theme';

export class UnComponente extends LitElement {
  static get is() {
    return "un-componente";
  }

  static get properties() {
    return {
      nombre: {
        type: String,
      },
      edad: {
        type: Number,
        attribute: "atributo-edad"
      },
      miHobby: {
        type: String,
        attribute: 'mi-hobby'
      },
    };
  }

  static styles = [
    theme,
    css`
      :host {
        display: block;
        color: var(--color-primary);
      }
    `
  ];

  constructor() {
    super()

    this.nombre = '<insertar nombre>'
    this.edad = '<insertar edad>'
    this.miHobby = '<insertar hobby>'
  }

  render() {
    return html`<h1>Hola! Soy ${this.nombre} y tengo ${this.edad} años y mi pasatiempo favorito es ${this.miHobby}</h1>`;
  }
}

window.customElements.define(UnComponente.is, UnComponente);
