import { LitElement, html, css } from 'lit';
import { theme } from '../styles/theme';
import { UnComponente } from './un-componente';

export class OtroComponente extends LitElement {
  static get is() {
    return "otro-componente";
  }

  static get properties() {
    return {};
  }
  static styles = [
    theme,
    css`
      :host {
        display: block;
        color: var(--color-primary);
      }

      td, .border {
        border: solid 3px;
      }

      .black-bg {
          background-color: #444;
          color: #fff;
          font-size: 20px;
      }

      .red {
          color: #f08;
      }

      .blue {
          color: #3af;
      }

      .green {
          color: #ae4;
      }

      .yellow {
          color: #ff0;
      }

      .error {
          background-color: #e00;
          color: #000;
      }

      .bracket {
          color: #c65;
      }
    `
  ];

  render() {
    return html `
    <div class="border">
      <table>
        <tr>
          <td>tipo</td>
          <td>nombre</td>
          <td>valor</td>
          <td>es renderizado?</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>nombre</td>
          <td>Miguel</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>atributo-edad</td>
          <td>24</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>mi-hobby</td>
          <td>cocinar pasteles</td>
          <td>Sí</td>
        </tr>
      </table>
      <pre class="prettyprint black-bg">
        &lt;<span class="red">un-componente</span>
          <span class="green">.nombre</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'Miguel'<span class="bracket">}</span>"</span>
          <span class="green">.edad</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>31<span class="bracket">}</span>"</span> 
          <span class="green">miHobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'cocinar pasteles'<span class="bracket">}</span>"</span> 
          <span class="green">style</span>=<span class="yellow">"color: #f33"</span>&gt;
        &lt;/<span class="red">un-componente</span>&gt;
        </pre>
      <un-componente .nombre="${'Miguel'}" .edad="${31}" .miHobby="${'cocinar pasteles'}" style="color: #f99">
      </un-componente>
    </div>
      
    <div class="border">
      <table>
        <tr>
          <td>tipo</td>
          <td>nombre</td>
          <td>valor</td>
          <td>es renderizado?</td>
        </tr>
        <tr>
          <td>propiedad</td>
          <td>nombre</td>
          <td>Miranda</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>edad</td>
          <td>27</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>mi-hobby</td>
          <td>salir con amistades</td>
          <td>Sí</td>
        </tr>
      </table>
      <pre class="prettyprint black-bg">
        &lt;<span class="red">un-componente</span>
          <span class="green">nombre</span>=<span class="yellow">"Miranda"</span>
          <span class="green">.edad</span>=<span class="yellow">"35"</span> 
          <span class="green">.miHobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'salir con amistades'<span class="bracket">}</span>"</span> 
          <span class="green">style</span>=<span class="yellow">"color: #8a8"</span>&gt;
        &lt;/<span class="red">un-componente</span>&gt;
      </pre>
      <un-componente nombre="Miranda" .edad="${35}" .miHobby="${'salir con amistades'}" style="color: #8a8">
      </un-componente>
    </div>

    <div class="border">
      <table>
        <tr>
          <td>tipo</td>
          <td>nombre</td>
          <td>valor</td>
          <td>es renderizado?</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>nombre</td>
          <td>Marcos</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>atributo-edad</td>
          <td>31</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>miHobby</td>
          <td>cocinar pizza</td>
          <td>No</td>
        </tr>
        <tr>
          <td>propiedad</td>
          <td>.miHobby</td>
          <td>cultivar jitomates</td>
          <td>Sí</td>
        </tr>
      </table>
      <pre class="prettyprint black-bg">
        &lt;<span class="red">un-componente</span>
          <span class="green">nombre</span>=<span class="yellow">"Marcos"</span>
          <span class="green">atributo-edad</span>=<span class="yellow">"41"</span> 
          <span class="green">mi-hobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'cultivar papas'<span class="bracket">}</span>"</span> 
          <span class="green">.miHobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'cultivar jitomates'<span class="bracket">}</span>"</span> 
          <span class="green">style</span>=<span class="yellow">"color: #8a8"</span>&gt;
        &lt;/<span class="red">un-componente</span>&gt;
      </pre>
      <un-componente nombre="Marcos" atributo-edad="41" mi-hobby="${'cultivar papas'}" .miHobby="${'cultivar jitomates'}" style="color: #888">
      </un-componente>
    </div>

    <div class="border">
      <table>
        <tr>
          <td>tipo</td>
          <td>nombre</td>
          <td>valor</td>
          <td>es renderizado?</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>nombre</td>
          <td>Selene</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>atributo-edad</td>
          <td>27</td>
          <td>Sí</td>
        </tr>
        <tr>
          <td>atributo</td>
          <td>miHobby</td>
          <td>cocinar tamales</td>
          <td>No</td>
        </tr>
      </table>
      <pre class="prettyprint black-bg">
        &lt;<span class="red">un-componente</span>
          <span class="green">nombre</span>=<span class="yellow">"Selene"</span>
          <span class="green">atributo-edad</span>=<span class="yellow">"27"</span> 
          <span class="error">miHobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'cultivar papas'<span class="bracket">}</span>"</span> 
          <span class="green">mi-hobby</span>=<span class="yellow">"<span class="red">$</span><span class="bracket">{</span>'cocinar tamales'<span class="bracket">}</span>"</span> 
          <span class="green">style</span>=<span class="yellow">"color: #8a8"</span>&gt;
        &lt;/<span class="red">un-componente</span>&gt;
      </pre>
      <un-componente nombre="Selene" atributo-edad="27" miHobby="cocinar tamales" mi-hobby="cocinar tamales">
      </un-componente>
    </div>
    `;
  }
}

window.customElements.define(OtroComponente.is, OtroComponente);
