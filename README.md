# test-app

Install dependencies

Using yarn:
```
yarn install
```

Using npm:
```
npm i
```

Globally install vite
```
npm i -g vite
```

Run using vite
```
vite
```

